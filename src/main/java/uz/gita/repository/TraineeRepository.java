package uz.gita.repository;

import uz.gita.model.Trainee;

import java.util.List;


public interface TraineeRepository {

    Trainee save(Trainee trainee);

    Trainee findById(Long id);

    void delete(Long id);

    List<Trainee> findAll();
}
