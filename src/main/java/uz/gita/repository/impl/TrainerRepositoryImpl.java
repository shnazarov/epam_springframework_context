package uz.gita.repository.impl;

import org.springframework.stereotype.Repository;
import uz.gita.model.Trainer;
import uz.gita.repository.TrainerRepository;
import uz.gita.storage.InMemoryStorage;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class TrainerRepositoryImpl implements TrainerRepository {
    private final Map<Long, Trainer> trainers;

    public TrainerRepositoryImpl(InMemoryStorage inMemoryStorage) {
        this.trainers = inMemoryStorage.getTrainerMap();
    }

    @Override
    public Trainer save(Trainer trainer) {
        Long id = trainer.getId();
        if (id == null || id == 0) {
            id = (long) (trainers.size() + 1);
            trainer.setId(id);
        }

        trainers.put(id, trainer);
        return trainer;
    }

    @Override
    public Trainer findById(Long id) {
        return trainers.get(id);
    }

    @Override
    public List<Trainer> findAll() {
        return new ArrayList<>(trainers.values());
    }
}
