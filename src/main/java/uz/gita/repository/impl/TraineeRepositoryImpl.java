package uz.gita.repository.impl;

import lombok.extern.java.Log;
import org.springframework.stereotype.Repository;
import uz.gita.model.Trainee;
import uz.gita.repository.TraineeRepository;
import uz.gita.storage.InMemoryStorage;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
@Log
public class TraineeRepositoryImpl implements TraineeRepository {

    private final Map<Long, Trainee> trainees;

    public TraineeRepositoryImpl(InMemoryStorage inMemoryStorage) {
        this.trainees = inMemoryStorage.getTraineeMap();
    }

    public Trainee save(Trainee trainee) {
        Long id = trainee.getId();
        if (id == null || id == 0) {
            id = (long) (trainees.size() + 1);
            trainee.setId(id);
        }
        log.info(trainee.toString());
        trainees.put(id, trainee);
        return trainee;
    }


    public Trainee findById(Long id) {
        return trainees.get(id);
    }


    public List<Trainee> findAll() {

        return new ArrayList<>(trainees.values());
    }

    public void delete(Long id) {
        trainees.get(id).getUser().setIsActive(false);
    }
}
