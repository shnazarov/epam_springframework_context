package uz.gita.repository.impl;

import org.springframework.stereotype.Repository;
import uz.gita.model.Training;
import uz.gita.repository.TrainingRepository;
import uz.gita.storage.InMemoryStorage;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class TrainingRepositoryImpl implements TrainingRepository {

    private final Map<Long, Training> trainings;

    public TrainingRepositoryImpl(InMemoryStorage inMemoryStorage) {
        this.trainings = inMemoryStorage.getTrainingMap();
    }

    @Override
    public Training save(Training training) {
        Long id = training.getId();
        if (id == null || id == 0) {
            id = (long) (trainings.size() + 1);
            training.setId(id);
        }

        trainings.put(id, training);
        return training;
    }

    @Override
    public Training findById(Long id) {
        return trainings.get(id);
    }

    @Override
    public List<Training> findAll() {
        return new ArrayList<>(trainings.values());
    }
}
