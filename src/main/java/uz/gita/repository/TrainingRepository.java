package uz.gita.repository;

import uz.gita.model.Training;

import java.util.List;

public interface TrainingRepository {

    Training save(Training training);

    Training findById(Long id);

    List<Training> findAll();
}
