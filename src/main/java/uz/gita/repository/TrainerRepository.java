package uz.gita.repository;

import uz.gita.model.Trainer;

import java.util.List;

public interface TrainerRepository {
    Trainer save(Trainer trainer);

    Trainer findById(Long id);

    List<Trainer> findAll();
}
