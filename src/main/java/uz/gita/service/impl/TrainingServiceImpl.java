package uz.gita.service.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import uz.gita.model.Training;
import uz.gita.repository.TrainingRepository;
import uz.gita.service.TrainingService;

import java.util.List;

@Service
@AllArgsConstructor
public class TrainingServiceImpl implements TrainingService {
    private TrainingRepository trainingRepository;

    @Override
    public Training create(Training training) {
        return trainingRepository.save(training);
    }

    @Override
    public List<Training> findAll() {
        return trainingRepository.findAll();
    }
}
