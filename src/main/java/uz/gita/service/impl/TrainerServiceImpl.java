package uz.gita.service.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import uz.gita.model.Trainer;
import uz.gita.repository.TrainerRepository;
import uz.gita.service.TrainerService;
import uz.gita.util.ProfileGenerator;

import java.util.List;

@Service
@AllArgsConstructor
public class TrainerServiceImpl implements TrainerService {
    private TrainerRepository trainerRepository;
    private ProfileGenerator profileGenerator;

    @Override
    public Trainer create(Trainer trainer) {
        trainer.setUser(profileGenerator.generateUserProfile(trainer.getUser()));
        return trainerRepository.save(trainer);
    }

    @Override
    public Trainer update(Trainer trainer) {
        return trainerRepository.save(trainer);
    }

    @Override
    public List<Trainer> findAll() {
        return trainerRepository.findAll().stream().filter(it -> it.getUser().getIsActive()).toList();
    }
}
