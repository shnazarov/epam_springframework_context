package uz.gita.service.impl;

import lombok.AllArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.stereotype.Service;
import uz.gita.model.Trainee;
import uz.gita.repository.TraineeRepository;
import uz.gita.service.TraineeService;
import uz.gita.util.ProfileGenerator;

import java.util.List;

@Service
@AllArgsConstructor
@Log
public class TraineeServiceImpl implements TraineeService {

    private TraineeRepository traineeRepository;
    private ProfileGenerator profileGenerator;

    @Override
    public Trainee create(Trainee trainee) {
        trainee.setUser(profileGenerator.generateUserProfile(trainee.getUser()));
        log.info(trainee.toString());
        return traineeRepository.save(trainee);
    }

    @Override
    public Trainee update(Trainee trainee) {
        return traineeRepository.save(trainee);
    }

    @Override
    public void delete(Long id) {
        traineeRepository.delete(id);
    }

    @Override
    public List<Trainee> findAll() {
        return traineeRepository.findAll().stream().filter(it -> it.getUser().getIsActive()).toList();
    }


}
