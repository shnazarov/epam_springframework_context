package uz.gita.service.impl;

import lombok.AllArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.stereotype.Service;
import uz.gita.model.Trainee;
import uz.gita.model.Trainer;
import uz.gita.model.Training;
import uz.gita.service.FacadeService;
import uz.gita.service.TraineeService;
import uz.gita.service.TrainerService;
import uz.gita.service.TrainingService;

import java.util.List;

@Service
@AllArgsConstructor
@Log
public class FacadeServiceImpl implements FacadeService {

    private final TrainingService trainingService;
    private final TraineeService traineeService;
    private final TrainerService trainerService;

    @Override
    public Trainee createTrainee(Trainee trainee) {
        log.info("facadeServiceImpl "+trainee.toString());
        return traineeService.create(trainee);
    }

    @Override
    public Trainee updateTrainee(Trainee trainee) {
        return traineeService.update(trainee);
    }

    @Override
    public void deleteTrainee(Long id) {
        traineeService.delete(id);
    }

    @Override
    public List<Trainee> findAllTrainees() {
        return traineeService.findAll();
    }

    @Override
    public Trainer createTrainer(Trainer trainer) {
        return trainerService.create(trainer);
    }

    @Override
    public Trainer updateTrainer(Trainer trainer) {
        return trainerService.update(trainer);
    }

    @Override
    public List<Trainer> findAllTrainers() {
        return trainerService.findAll();
    }

    @Override
    public Training createTraining(Training training) {
        return trainingService.create(training);
    }

    @Override
    public List<Training> findAllTrainings() {
        return trainingService.findAll();
    }
}
