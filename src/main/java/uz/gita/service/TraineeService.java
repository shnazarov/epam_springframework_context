package uz.gita.service;

import uz.gita.model.Trainee;

import java.util.List;

public interface TraineeService {

    Trainee create(Trainee trainee);

    Trainee update(Trainee trainee);

    void delete(Long id);

    List<Trainee> findAll();
}
