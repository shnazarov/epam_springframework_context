package uz.gita.service;

import uz.gita.model.Trainee;
import uz.gita.model.Trainer;
import uz.gita.model.Training;

import java.util.List;

public interface FacadeService {

    Trainee createTrainee(Trainee trainee);

    Trainee updateTrainee(Trainee trainee);

    void deleteTrainee(Long id);

    List<Trainee> findAllTrainees();

    Trainer createTrainer(Trainer trainer);

    Trainer updateTrainer(Trainer trainer);

    List<Trainer> findAllTrainers();

    Training createTraining(Training training);

    List<Training> findAllTrainings();
}
