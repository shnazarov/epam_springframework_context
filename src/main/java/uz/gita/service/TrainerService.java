package uz.gita.service;

import uz.gita.model.Trainer;

import java.util.List;

public interface TrainerService {

    Trainer create(Trainer trainer);

    Trainer update(Trainer trainer);

    List<Trainer> findAll();
}
