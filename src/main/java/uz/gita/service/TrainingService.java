package uz.gita.service;

import uz.gita.model.Training;

import java.util.List;

public interface TrainingService {

    Training create(Training training);

    List<Training> findAll();
}
