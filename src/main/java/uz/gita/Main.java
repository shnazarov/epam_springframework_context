package uz.gita;

import lombok.extern.java.Log;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import uz.gita.config.SpringConfig;
import uz.gita.storage.InMemoryStorage;


@Log
public class Main {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(SpringConfig.class);

        var storage = context.getBean(InMemoryStorage.class);
    }
}

