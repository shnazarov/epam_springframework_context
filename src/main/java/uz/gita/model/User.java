package uz.gita.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User {
    private String firstname;
    private String lastname;

    private String username;
    private String password;
    private Boolean isActive;
}
