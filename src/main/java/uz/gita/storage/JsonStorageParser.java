package uz.gita.storage;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import uz.gita.model.Trainee;
import uz.gita.model.Trainer;
import uz.gita.model.TrainingType;
import uz.gita.model.User;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Component
@Log
public class JsonStorageParser {

    @Value("${trainees}")
    private String traineesPath;

    @Value("${trainers}")
    private String trainersPath;

    public Map<Long, Trainee> readTraineeStorage() {
        Map<Long, Trainee> trainees = new HashMap<>();

        // Read the JSON file.
        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = null;
        try {
            root = mapper.readTree(new File(traineesPath));
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Iterate over the JSON object and create a Trainer object for each key-value pair.
        assert root != null;
        for (JsonNode trainerNode : root.get("trainees")) {
            Long id = trainerNode.get("id").asLong();

            // Create a User object from the JSON object.
            User user = new User(
                    trainerNode.get("user").get("firstname").asText(),
                    trainerNode.get("user").get("lastname").asText(),
                    trainerNode.get("user").get("username").asText(),
                    trainerNode.get("user").get("password").asText(),
                    trainerNode.get("user").get("isActive").asBoolean()
            );

            // Create a Trainer object and add it to the map.
            Trainee trainee = new Trainee(id, user);
            trainees.put(id, trainee);
        }

        //log.info(trainees.toString());
        return trainees;
    }


    public Map<Long, Trainer> readTrainersStorage() {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = null;
        try {
            root = mapper.readTree(new File(trainersPath));
        } catch (IOException e) {
            e.printStackTrace();
        }

        Map<Long, Trainer> trainers = new HashMap<>();

        assert root != null;
        for (JsonNode trainerNode : root.get("trainers")) {
            Long id = trainerNode.get("id").asLong();
            String specialization = trainerNode.get("specialization").asText();

            JsonNode userNode = trainerNode.get("user");
            String firstname = userNode.get("firstname").asText();
            String lastname = userNode.get("lastname").asText();
            String username = userNode.get("username").asText();
            String password = userNode.get("password").asText();
            Boolean isActive = userNode.get("isActive").asBoolean();

            User user = new User(firstname, lastname, username, password, isActive);
            TrainingType trainingType = new TrainingType(specialization);
            Trainer trainer = new Trainer(id, trainingType, user);

            trainers.put(id, trainer);
        }

        return trainers;
    }
}
