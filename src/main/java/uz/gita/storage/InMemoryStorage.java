package uz.gita.storage;


import jakarta.annotation.PostConstruct;
import lombok.Getter;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import uz.gita.model.*;


import java.util.HashMap;
import java.util.Map;

@Component
@Log
@Getter
public class InMemoryStorage {
    private final Map<Long, Trainee> traineeMap = new HashMap<>();
    private final Map<Long, Trainer> trainerMap = new HashMap<>();
    private final Map<Long, Training> trainingMap = new HashMap<>();

    @Autowired
    private JsonStorageParser parser;

    @PostConstruct
    private void init() {
        traineeMap.putAll(parser.readTraineeStorage());
        trainerMap.putAll(parser.readTrainersStorage());
       // log.info("postConstruct for traineeMap: " + this.getTraineeMap());
        //log.info("postConstruct for trainerMap: " + this.getTrainerMap());
    }
}
