package uz.gita.util;

import org.springframework.stereotype.Component;
import uz.gita.model.User;

import java.security.SecureRandom;
import java.util.StringJoiner;
import java.util.UUID;

@Component
public class ProfileGenerator {

    public User generateUserProfile(User user) {
        if (user.getUsername() == null || user.getUsername().isEmpty()) {
            var username = new StringJoiner(".").add(user.getFirstname()).add(user.getLastname()).toString();
            user.setUsername(username);
        }

        if (user.getPassword() == null || user.getPassword().isEmpty()) {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()-_=+";
            StringBuilder password = new StringBuilder();
            SecureRandom random = new SecureRandom();

            for (int i = 0; i < 10; i++) {
                int randomIndex = random.nextInt(chars.length());
                password.append(chars.charAt(randomIndex));
            }

            user.setPassword(password.toString());
        }

        return user;
    }
}
