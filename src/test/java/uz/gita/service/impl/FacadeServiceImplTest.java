package uz.gita.service.impl;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import uz.gita.config.SpringConfig;
import uz.gita.model.*;
import uz.gita.service.FacadeService;
import uz.gita.service.TraineeService;
import uz.gita.service.TrainerService;
import uz.gita.service.TrainingService;

import java.time.LocalDate;
import java.time.Month;
import java.time.MonthDay;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class FacadeServiceImplTest {


    @InjectMocks
    FacadeServiceImpl facadeService;

    @Mock
    TraineeService traineeService;

    @Mock
    TrainerService trainerService;

    @Mock
    TrainingService trainingService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void createTrainee() {
        var trainee1 = new Trainee(0L, new User("Alice", "Robinson", null, null, true));
        var trainee2 = new Trainee(0L, new User("Bob", "Griffin", null, null, true));

        facadeService.createTrainee(trainee1);
        facadeService.createTrainee(trainee2);

        verify(traineeService, times(2)).create(any());

    }

    @Test
    void updateTrainee() {
        var trainee1 = new Trainee(0L, new User("Azamat", "Kali", null, null, true));
        var trainee2 = new Trainee(0L, new User("Sevara", "Sevara", null, null, true));

        facadeService.updateTrainee(trainee1);
        facadeService.updateTrainee(trainee2);

        verify(traineeService, times(2)).update(any());
    }

    @Test
    void deleteTrainee() {
        Long id = 1L;
        Long id2 = 2L;

        facadeService.deleteTrainee(id);
        facadeService.deleteTrainee(id2);

        verify(traineeService, times(2)).delete(any());
    }

    @Test
    void findAllTrainees() {
        List<Trainee> trainees = new ArrayList<>();
        var trainee1 = new Trainee(1L, new User("Alice", "Robinson", null, null, true));
        var trainee2 = new Trainee(2L, new User("Bob", "Griffin", null, null, true));
        var trainee3 = new Trainee(3L, new User("Azamat", "Kali", null, null, true));
        var trainee4 = new Trainee(4L, new User("Sevara", "Sevara", null, null, true));

        trainees.add(trainee1);
        trainees.add(trainee2);
        trainees.add(trainee3);
        trainees.add(trainee4);

        when(traineeService.findAll()).thenReturn(trainees);

        List<Trainee> traineeList = facadeService.findAllTrainees();

        assertNotNull(traineeList);
        assertEquals(4, trainees.size());
        assertEquals("Alice", traineeList.get(0).getUser().getFirstname());
        assertEquals(true, traineeList.get(3).getUser().getIsActive());

        assertEquals("Sevara", traineeList.get(3).getUser().getFirstname());
        assertEquals("Kali", traineeList.get(2).getUser().getLastname());
    }

    @Test
    void createTrainer() {
        var trainer1 = new Trainer(0L, new TrainingType("Fitness"), new User("Trainer1", "Trainerov", null, null, true));
        var trainer2 = new Trainer(0L, new TrainingType("BodyBuilding"), new User("Trainer2", "Trainerov2", null, null, true));

        facadeService.createTrainer(trainer1);
        facadeService.createTrainer(trainer2);

        verify(trainerService, times(2)).create(any());
    }

    @Test
    void updateTrainer() {
        var trainer1 = new Trainer(0L, new TrainingType("Fitness"), new User("Trainer1", "Trainerov", null, null, true));
        var trainer2 = new Trainer(0L, new TrainingType("BodyBuilding"), new User("Trainer2", "Trainerov2", null, null, true));

        facadeService.updateTrainer(trainer1);
        facadeService.updateTrainer(trainer2);

        verify(trainerService, times(2)).update(any());
    }

    @Test
    void findAllTrainers() {

        List<Trainer> trainers = new ArrayList<>();
        var trainer1 = new Trainer(1L, new TrainingType("Fitness"), new User("Alice", "Robinson", null, null, true));
        var trainer2 = new Trainer(2L, new TrainingType("BodyBuilding"), new User("Bob", "Griffin", null, null, true));
        var trainer3 = new Trainer(3L, new TrainingType("Shaping"), new User("Azamat", "Kali", null, null, true));
        var trainer4 = new Trainer(4L, new TrainingType("Swimming"), new User("Sevara", "Sevara", null, null, true));

        trainers.add(trainer1);
        trainers.add(trainer2);
        trainers.add(trainer3);
        trainers.add(trainer4);

        when(trainerService.findAll()).thenReturn(trainers);

        List<Trainer> trainerList = facadeService.findAllTrainers();

        assertNotNull(trainerList);
        assertEquals(4, trainers.size());
        assertEquals("Alice", trainerList.get(0).getUser().getFirstname());
        assertEquals(true, trainerList.get(3).getUser().getIsActive());

        assertEquals("Sevara", trainerList.get(3).getUser().getFirstname());
        assertEquals("Kali", trainerList.get(2).getUser().getLastname());
    }


    @Test
    void createTraining() {
        var trainee1 = new Trainee(1L, new User("Alice", "Robinson", null, null, true));
        var trainee2 = new Trainee(2L, new User("Bob", "Griffin", null, null, true));

        var trainer1 = new Trainer(1L, new TrainingType("Fitness"), new User("Gary", "Fisherman", null, null, true));
        var trainer2 = new Trainer(2L, new TrainingType("BodyBuilding"), new User("Zaid", "Gold", null, null, true));

        var training1 = Training.builder()
                .id(1L)
                .trainee(trainee1)
                .trainer(trainer1)
                .trainingName("TrainingName1")
                .trainingType(new TrainingType("Fitness"))
                .trainingDate(LocalDate.of(2023, Month.APRIL, 10))
                .durationInHours(2L)
                .build();

        var training2 = Training.builder()
                .id(2L)
                .trainee(trainee2)
                .trainer(trainer2)
                .trainingName("TrainingName2")
                .trainingType(new TrainingType("BodyBuilding"))
                .trainingDate(LocalDate.of(2023, Month.AUGUST, 1))
                .durationInHours(1L)
                .build();

        facadeService.createTraining(training1);
        facadeService.createTraining(training2);

        verify(trainingService, times(2)).create(any());
    }

    @Test
    void findAllTrainings() {
        var trainee1 = new Trainee(1L, new User("Alice", "Robinson", null, null, true));
        var trainee2 = new Trainee(2L, new User("Bob", "Griffin", null, null, true));

        var trainer1 = new Trainer(1L, new TrainingType("Fitness"), new User("Gary", "Fisherman", null, null, true));
        var trainer2 = new Trainer(2L, new TrainingType("BodyBuilding"), new User("Zaid", "Gold", null, null, true));

        var training1 = Training.builder()
                .id(1L)
                .trainee(trainee1)
                .trainer(trainer1)
                .trainingName("TrainingName1")
                .trainingType(new TrainingType("Fitness"))
                .trainingDate(LocalDate.of(2023, Month.APRIL, 10))
                .durationInHours(2L)
                .build();

        var training2 = Training.builder()
                .id(2L)
                .trainee(trainee2)
                .trainer(trainer2)
                .trainingName("TrainingName2")
                .trainingType(new TrainingType("BodyBuilding"))
                .trainingDate(LocalDate.of(2023, Month.AUGUST, 1))
                .durationInHours(1L)
                .build();

        var trainings = new ArrayList<Training>();

        trainings.add(training1);
        trainings.add(training2);

        when(trainingService.findAll()).thenReturn(trainings);

        var trainingsList = facadeService.findAllTrainings();
        assertNotNull(trainingsList);

        assertEquals(1L, trainingsList.get(0).getId());
        assertEquals(trainee1, trainingsList.get(0).getTrainee());
        assertEquals(trainer2, trainingsList.get(1).getTrainer());

        assertEquals("TrainingName2", trainingsList.get(1).getTrainingName());
    }

}