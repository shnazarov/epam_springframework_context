package uz.gita.service.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import uz.gita.model.*;
import uz.gita.repository.TrainingRepository;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class TrainingServiceImplTest {
    @InjectMocks
    TrainingServiceImpl trainingService;

    @Mock
    TrainingRepository trainingRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void create() {
        var trainee1 = new Trainee(1L, new User("Alice", "Robinson", null, null, true));
        var trainee2 = new Trainee(2L, new User("Bob", "Griffin", null, null, true));

        var trainer1 = new Trainer(1L, new TrainingType("Fitness"), new User("Gary", "Fisherman", null, null, true));
        var trainer2 = new Trainer(2L, new TrainingType("BodyBuilding"), new User("Zaid", "Gold", null, null, true));

        var training1 = Training.builder()
                .id(1L)
                .trainee(trainee1)
                .trainer(trainer1)
                .trainingName("TrainingName1")
                .trainingType(new TrainingType("Fitness"))
                .trainingDate(LocalDate.of(2023, Month.APRIL, 10))
                .durationInHours(2L)
                .build();

        var training2 = Training.builder()
                .id(2L)
                .trainee(trainee2)
                .trainer(trainer2)
                .trainingName("TrainingName2")
                .trainingType(new TrainingType("BodyBuilding"))
                .trainingDate(LocalDate.of(2023, Month.AUGUST, 1))
                .durationInHours(1L)
                .build();

        trainingService.create(training1);
        trainingService.create(training2);

        verify(trainingRepository, times(2)).save(any());
    }

    @Test
    void findAll() {
        var trainee1 = new Trainee(1L, new User("Alice", "Robinson", null, null, true));
        var trainee2 = new Trainee(2L, new User("Bob", "Griffin", null, null, true));

        var trainer1 = new Trainer(1L, new TrainingType("Fitness"), new User("Gary", "Fisherman", null, null, true));
        var trainer2 = new Trainer(2L, new TrainingType("BodyBuilding"), new User("Zaid", "Gold", null, null, true));

        var training1 = Training.builder()
                .id(1L)
                .trainee(trainee1)
                .trainer(trainer1)
                .trainingName("TrainingName1")
                .trainingType(new TrainingType("Fitness"))
                .trainingDate(LocalDate.of(2023, Month.APRIL, 10))
                .durationInHours(2L)
                .build();

        var training2 = Training.builder()
                .id(2L)
                .trainee(trainee2)
                .trainer(trainer2)
                .trainingName("TrainingName2")
                .trainingType(new TrainingType("BodyBuilding"))
                .trainingDate(LocalDate.of(2023, Month.AUGUST, 1))
                .durationInHours(1L)
                .build();

        var trainings = new ArrayList<Training>();

        trainings.add(training1);
        trainings.add(training2);

        when(trainingService.findAll()).thenReturn(trainings);

        var trainingsList = trainingService.findAll();
        assertNotNull(trainingsList);

        assertEquals(1L, trainingsList.get(0).getId());
        assertEquals(trainee1, trainingsList.get(0).getTrainee());
        assertEquals(trainer2, trainingsList.get(1).getTrainer());

        assertEquals("TrainingName2", trainingsList.get(1).getTrainingName());
    }
}