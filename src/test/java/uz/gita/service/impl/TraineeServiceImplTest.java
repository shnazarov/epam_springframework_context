package uz.gita.service.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import uz.gita.model.Trainee;
import uz.gita.model.Training;
import uz.gita.model.User;
import uz.gita.repository.TraineeRepository;
import uz.gita.service.TraineeService;
import uz.gita.util.ProfileGenerator;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class TraineeServiceImplTest {

    @InjectMocks
    TraineeServiceImpl traineeService;

    @Mock
    TraineeRepository traineeRepository;

    @Mock
    ProfileGenerator profileGenerator;


    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void create() {
        var trainee1 = new Trainee(0L, new User("Alice", "Robinson", null, null, true));
        var trainee2 = new Trainee(0L, new User("Bob", "Griffin", null, null, true));

        //var userProfileResult = new User("Alice", "Robinson", "Alice.Robinson", "1234qwerty", true);
        traineeService.create(trainee1);
        traineeService.create(trainee2);

        verify(traineeRepository, times(2)).save(any());
        verify(profileGenerator, times(2)).generateUserProfile(any());

    }

    @Test
    void update() {
        var trainee1 = new Trainee(0L, new User("Azamat", "Kali", null, null, true));
        var trainee2 = new Trainee(0L, new User("Sevara", "Sevara", null, null, true));

        traineeService.update(trainee1);
        traineeService.update(trainee2);

        verify(traineeRepository, times(2)).save(any());
    }

    @Test
    void delete() {
        var trainee1 = new Trainee(1L, new User("Azamat", "Kali", null, null, true));
        var trainee2 = new Trainee(2L, new User("Sevara", "Sevara", null, null, true));


        Long id = 1L;
        Long id2 = 2L;

        when(traineeRepository.findById(id)).thenReturn(trainee1);
        when(traineeRepository.findById(id2)).thenReturn(trainee2);

        traineeService.delete(id);
        traineeService.delete(id2);

        verify(traineeRepository, times(2)).delete(any());
    }

    @Test
    void findAll() {
        List<Trainee> trainees = new ArrayList<>();
        var trainee1 = new Trainee(1L, new User("Alice", "Robinson", null, null, true));
        var trainee2 = new Trainee(2L, new User("Bob", "Griffin", null, null, true));
        var trainee3 = new Trainee(3L, new User("Azamat", "Kali", null, null, true));
        var trainee4 = new Trainee(4L, new User("Sevara", "Sevara", null, null, true));

        trainees.add(trainee1);
        trainees.add(trainee2);
        trainees.add(trainee3);
        trainees.add(trainee4);

        when(traineeRepository.findAll()).thenReturn(trainees);

        List<Trainee> traineeList = traineeService.findAll();

        assertNotNull(traineeList);
        assertEquals(4, trainees.size());
        assertEquals("Alice", traineeList.get(0).getUser().getFirstname());
        assertEquals(true, traineeList.get(3).getUser().getIsActive());

        assertEquals("Sevara", traineeList.get(3).getUser().getFirstname());
        assertEquals("Kali", traineeList.get(2).getUser().getLastname());
    }
}