package uz.gita.service.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import uz.gita.model.Trainer;
import uz.gita.model.TrainingType;
import uz.gita.model.User;
import uz.gita.repository.TrainerRepository;
import uz.gita.util.ProfileGenerator;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class TrainerServiceImplTest {
    @InjectMocks
    TrainerServiceImpl trainerService;

    @Mock
    TrainerRepository trainerRepository;

    @Mock
    ProfileGenerator profileGenerator;

    @BeforeEach
    void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void create() {
        var trainer1 = new Trainer(0L, new TrainingType("Fitness"), new User("Trainer1", "Trainerov", null, null, true));
        var trainer2 = new Trainer(0L, new TrainingType("BodyBuilding"), new User("Trainer2", "Trainerov2", null, null, true));

        trainerService.create(trainer1);
        trainerService.create(trainer2);

        verify(trainerRepository, times(2)).save(any());
    }

    @Test
    void update() {
        var trainer1 = new Trainer(0L, new TrainingType("Fitness"), new User("Trainer1", "Trainerov", null, null, true));
        var trainer2 = new Trainer(0L, new TrainingType("BodyBuilding"), new User("Trainer2", "Trainerov2", null, null, true));

        trainerService.update(trainer1);
        trainerService.update(trainer2);

        verify(trainerRepository, times(2)).save(any());
    }

    @Test
    void findAll() {
        List<Trainer> trainers = new ArrayList<>();
        var trainer1 = new Trainer(1L, new TrainingType("Fitness"), new User("Alice", "Robinson", null, null, true));
        var trainer2 = new Trainer(2L, new TrainingType("BodyBuilding"), new User("Bob", "Griffin", null, null, true));
        var trainer3 = new Trainer(3L, new TrainingType("Shaping"), new User("Azamat", "Kali", null, null, true));
        var trainer4 = new Trainer(4L, new TrainingType("Swimming"), new User("Sevara", "Sevara", null, null, true));

        trainers.add(trainer1);
        trainers.add(trainer2);
        trainers.add(trainer3);
        trainers.add(trainer4);

        when(trainerService.findAll()).thenReturn(trainers);

        List<Trainer> trainerList = trainerService.findAll();

        assertNotNull(trainerList);
        assertEquals(4, trainers.size());
        assertEquals("Alice", trainerList.get(0).getUser().getFirstname());
        assertEquals(true, trainerList.get(3).getUser().getIsActive());

        assertEquals("Sevara", trainerList.get(3).getUser().getFirstname());
        assertEquals("Kali", trainerList.get(2).getUser().getLastname());
    }
}